DROP TABLE IF EXISTS item;

CREATE TABLE 'item' (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value VARCHAR(32) NOT NULL
);

INSERT INTO item (value) VALUES
("Test1"),
("Test2"),
("Test3"),
("Test4"),
("Test5"),
("Test6"),
("Test7"),
("Test8"),
("Test9");