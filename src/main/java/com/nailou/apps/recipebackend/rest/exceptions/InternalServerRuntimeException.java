package com.nailou.apps.recipebackend.rest.exceptions;

public class InternalServerRuntimeException extends APIRuntimeException {

    public InternalServerRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
