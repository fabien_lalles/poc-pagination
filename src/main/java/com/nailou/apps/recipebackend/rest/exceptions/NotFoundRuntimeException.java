package com.nailou.apps.recipebackend.rest.exceptions;

public class NotFoundRuntimeException extends APIRuntimeException {

    public NotFoundRuntimeException(final String message) {
        super(message);
    }
}
