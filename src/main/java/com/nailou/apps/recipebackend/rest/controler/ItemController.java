package com.nailou.apps.recipebackend.rest.controler;

import com.nailou.apps.recipebackend.common.utils.Paging;
import com.nailou.apps.recipebackend.common.utils.PagingUtils;
import com.nailou.apps.recipebackend.dao.db.ItemDAO;
import com.nailou.apps.recipebackend.dao.db.dto.ItemDTO;
import com.nailou.apps.recipebackend.dao.db.dto.PagedResult;
import com.nailou.apps.recipebackend.rest.exceptions.APIRuntimeException;
import com.nailou.apps.recipebackend.rest.exceptions.InternalServerRuntimeException;
import com.nailou.apps.recipebackend.rest.utils.HeaderUtils;
import com.nailou.apps.recipebackend.rest.utils.ItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/items")
public class ItemController {

    public static final Integer MAX_ITEM_PER_PAGE = 10;

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);

    private final ItemDAO itemDAO;

    private final ItemMapper itemMapper;

    public ItemController(final ItemDAO itemDAO,
                          final ItemMapper itemMapper) {
        this.itemDAO = itemDAO;
        this.itemMapper = itemMapper;
    }

    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<ItemDTO>> getAllItems(@RequestParam(value = "range", required = false) final String range) {
        try {
            LOGGER.debug("Call getAllItems methods");

            final Paging paging = PagingUtils.buildPage(range, MAX_ITEM_PER_PAGE);
            final PagedResult<ItemDTO> pagedResult = itemDAO.getAllWithRange(paging);

            return ResponseEntity.status(pagedResult.getTotalResults() == pagedResult.getResults().size() ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT)
                    .header("Content-Range", HeaderUtils.buildContentRange(paging, pagedResult))
                    .header("Accept-Ranges",HeaderUtils.buildContentType("item", 50) )
                    .body(pagedResult.getResults());
        } catch (final APIRuntimeException apiRuntimeException) {
            throw apiRuntimeException;
        } catch (final Exception exception) {
            throw new InternalServerRuntimeException("An unknown error occurred while handle item call.", exception);
        }
    }

//    @GetMapping(path = "/{itemId}", produces = {MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity<Item> getItemById(@PathParam("itemId") final Integer itemId) {
//        return itemRepository.findById(itemId)
//                .map(value -> ResponseEntity.ok().body(itemMapper.toItem(value)))
//                .orElseThrow(() -> new NotFoundRuntimeException("No item found with id : " + itemId));
//    }
//
//    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity<Item> postItem(@RequestBody final Item item) {
//        try {
//            final Item storedItem = itemMapper.toItem(itemRepository.save(itemMapper.toItemDTO(item)));
//
//            return ResponseEntity.created(new URI("/api/v1/items/" + storedItem.getId()))
//                    .body(storedItem);
//        } catch (final URISyntaxException uriSyntaxException) {
//            throw new InternalServerRuntimeException("An error occurred while creating URI Location for item creation.", uriSyntaxException);
//        }
//    }
//
//    @DeleteMapping(path = "/{itemId}", produces = {MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity<Item> deleteItem(@PathVariable("itemId") final Integer itemId) {
//        return itemRepository.findById(itemId)
//                .map(value -> {
//                    itemRepository.delete(value);
//
//                    return ResponseEntity.ok().body(itemMapper.toItem(value));
//                })
//                .orElseThrow(() -> new NotFoundRuntimeException("No item found with id : " + itemId));
//    }

}


