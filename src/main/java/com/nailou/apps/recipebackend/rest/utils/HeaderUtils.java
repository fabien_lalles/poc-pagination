package com.nailou.apps.recipebackend.rest.utils;

import com.nailou.apps.recipebackend.common.utils.Paging;
import com.nailou.apps.recipebackend.dao.db.dto.PagedResult;

public class HeaderUtils {

    private HeaderUtils() {}

    public static <T> String buildContentRange(final Paging paging,
                                               final PagedResult<T> pagedResult) {
        return pagedResult.getResults().isEmpty() ?
                "* / " + pagedResult.totalResults() : buildContentRangeWithResult(paging, pagedResult);
    }

    private static <T> String buildContentRangeWithResult(final Paging paging, final PagedResult<T> pagedResult) {
        final int totalResults = pagedResult.totalResults();
        final int totalResultsFound = pagedResult.getResults().size();

        return paging.direction().isAscending() ? paging.offset() + "-" + (paging.offset() + totalResultsFound - 1) + " / " + totalResults
                : (totalResults - totalResultsFound - 1) + "-" + (totalResults - 1) + " / " + totalResults;
    }

    public static String buildContentType(final String contentType, final int maxResults) {
        return contentType + " " + maxResults;
    }
}
