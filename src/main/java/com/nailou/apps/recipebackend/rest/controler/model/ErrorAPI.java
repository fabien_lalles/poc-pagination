package com.nailou.apps.recipebackend.rest.controler.model;

import org.springframework.http.HttpStatus;

public class ErrorAPI {

    private final HttpStatus httpStatus;

    private final String message;

    public ErrorAPI(final HttpStatus httpStatus,
                    final String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }

}
