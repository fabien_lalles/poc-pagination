package com.nailou.apps.recipebackend.rest.controler.model;

public class Item {

    private Integer id;

    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
