package com.nailou.apps.recipebackend.rest.utils;

import com.nailou.apps.recipebackend.rest.controler.model.ErrorAPI;
import com.nailou.apps.recipebackend.rest.exceptions.AskForToMuchResultRuntimeException;
import com.nailou.apps.recipebackend.rest.exceptions.IllegalRangeRuntimeException;
import com.nailou.apps.recipebackend.rest.exceptions.NotFoundRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RuntimeExceptionAdvice extends ResponseEntityExceptionHandler {

    /**
     * Handle any thrown {@link NotFoundRuntimeException}.
     *
     * @param runtimeException The exception thrown.
     * @param request The initial request.
     * @return A {@link ResponseEntity} with formatted message.
     */
    @SuppressWarnings("unused")
    @ExceptionHandler(value = {NotFoundRuntimeException.class})
    protected ResponseEntity<String> handleNotFoundRuntimeException(final Exception runtimeException,
                                                    final WebRequest request) {

        return new ResponseEntity<>(runtimeException.getMessage(), HttpStatus.NOT_FOUND);
    }

    /**
     * Handle any thrown {@link IllegalRangeRuntimeException}.
     *
     * @param runtimeException The exception thrown.
     * @param request The initial request.
     * @return A {@link ResponseEntity} with formatted message.
     */
    @SuppressWarnings("unused")
    @ExceptionHandler(value = {IllegalRangeRuntimeException.class})
    protected ResponseEntity<ErrorAPI> handleIllegalRangeRuntimeException(final Exception runtimeException,
                                                                        final WebRequest request) {
        return new ResponseEntity<>(new ErrorAPI(HttpStatus.BAD_REQUEST, runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle any thrown {@link AskForToMuchResultRuntimeException}.
     *
     * @param runtimeException The exception thrown.
     * @param request The initial request.
     * @return A {@link ResponseEntity} with formatted message.
     */
    @SuppressWarnings("unused")
    @ExceptionHandler(value = {AskForToMuchResultRuntimeException.class})
    protected ResponseEntity<ErrorAPI> handleAskForToMuchResultRuntimeException(final Exception runtimeException,
                                                                                final WebRequest request) {
        return new ResponseEntity<>(new ErrorAPI(HttpStatus.BAD_REQUEST, runtimeException.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
