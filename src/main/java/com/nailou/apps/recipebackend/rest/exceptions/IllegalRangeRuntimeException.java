package com.nailou.apps.recipebackend.rest.exceptions;

public class IllegalRangeRuntimeException extends APIRuntimeException {

    public IllegalRangeRuntimeException(final String message) {
        super(message);
    }
}
