package com.nailou.apps.recipebackend.rest.exceptions;

public abstract class APIRuntimeException extends RuntimeException {

    public APIRuntimeException(final String message) {
        super(message);
    }

    public APIRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
