package com.nailou.apps.recipebackend.rest.utils;

import com.nailou.apps.recipebackend.dao.db.dto.ItemDTO;
import com.nailou.apps.recipebackend.rest.controler.model.Item;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ItemMapper {

    List<Item> toItemList(final List<ItemDTO> itemDTOList);

    ItemDTO toItemDTO(final Item item);

    Item toItem(final ItemDTO itemDTO);
}
