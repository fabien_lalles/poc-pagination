package com.nailou.apps.recipebackend.rest.exceptions;

public class AskForToMuchResultRuntimeException extends APIRuntimeException {

    public AskForToMuchResultRuntimeException(final String message) {
        super(message);
    }
}
