package com.nailou.apps.recipebackend.dao.db;

import com.nailou.apps.recipebackend.common.utils.Paging;
import com.nailou.apps.recipebackend.dao.db.dto.ItemDTO;
import com.nailou.apps.recipebackend.dao.db.dto.PagedResult;
import com.nailou.apps.recipebackend.dao.exceptions.DAOException;

public interface ItemDAO {

    PagedResult<ItemDTO> getAllWithRange(final Paging paging) throws DAOException;
}
