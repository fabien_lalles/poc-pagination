package com.nailou.apps.recipebackend.dao.db.impl;

import com.nailou.apps.recipebackend.common.utils.Paging;
import com.nailou.apps.recipebackend.dao.db.ItemDAO;
import com.nailou.apps.recipebackend.dao.db.dto.ItemDTO;
import com.nailou.apps.recipebackend.dao.db.dto.PagedResult;
import com.nailou.apps.recipebackend.dao.exceptions.DAOException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Component
public class ItemDAOImpl implements ItemDAO {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ItemDAOImpl(final NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public PagedResult<ItemDTO> getAllWithRange(final Paging paging) throws DAOException {

        int totalResults;
        List<ItemDTO> dtoList;

        try {
            //noinspection ConstantConditions
            totalResults = namedParameterJdbcTemplate.queryForObject("SELECT COUNT(*) as total FROM item", new HashMap<>(), Integer.class);

            dtoList = new ArrayList<>(namedParameterJdbcTemplate.query(String.format("SELECT * FROM item ORDER BY ID %s OFFSET %s FETCH FIRST %s ROWS ONLY", paging.direction(), paging.offset(), paging.limit()), (rs, rowNum) -> {
                final ItemDTO itemDTO = new ItemDTO();

                itemDTO.setId(rs.getInt("ID"));
                itemDTO.setValue(rs.getString("VALUE"));

                return itemDTO;
            }));
        } catch (final EmptyResultDataAccessException emptyResultDataAccessException) {
            totalResults = 0;
            dtoList = Collections.emptyList();
        } catch (final NullPointerException | DataAccessException exception) {
            throw new DAOException("Cannot count total items", exception);
        }

        return new PagedResult<>(totalResults, dtoList);
    }
}
