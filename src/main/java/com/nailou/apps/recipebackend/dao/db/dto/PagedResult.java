package com.nailou.apps.recipebackend.dao.db.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

public record PagedResult<T>(int totalResults, List<T> results) {

    public int getTotalResults() {
        return totalResults;
    }

    public List<T> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("totalResults", totalResults)
                .append("results", results)
                .toString();
    }
}
