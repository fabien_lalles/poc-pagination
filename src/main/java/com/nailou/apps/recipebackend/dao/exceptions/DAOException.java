package com.nailou.apps.recipebackend.dao.exceptions;

public class DAOException extends Exception {

    public DAOException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
