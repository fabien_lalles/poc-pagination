package com.nailou.apps.recipebackend.common.utils;

import com.nailou.apps.recipebackend.rest.exceptions.AskForToMuchResultRuntimeException;
import com.nailou.apps.recipebackend.rest.exceptions.IllegalRangeRuntimeException;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.regex.Pattern;

public class PagingUtils {

    private static final String RANGE_SEPARATOR = "-";

    private static final Pattern NORMAL_RANGE_PATTERN = Pattern.compile("^[0-9]{1,3}-[0-9]{1,3}$");

    private static final Pattern LAST_RESULTS_PATTERN = Pattern.compile("^-[0-9]{1,3}$");

    private PagingUtils() {}

    public static Paging buildPage(final String range, final int maxResultAllowed) {
        if (!StringUtils.hasLength(range) ) {
            throw new IllegalRangeRuntimeException("Range cannot be null or empty. Range : " + range);
        }

        final Paging paging;

        if (NORMAL_RANGE_PATTERN.matcher(range).matches()) {
            final Integer[] splitRange = Arrays.stream(range.split(RANGE_SEPARATOR))
                        .map(Integer::parseInt)
                        .toArray(Integer[]::new);

            final int minCursor = splitRange[0];
            final int maxCursor = splitRange[1];

            if (maxCursor > minCursor) {
                paging = Paging.standardResult(minCursor, maxCursor - minCursor + 1);
            } else {
                paging = Paging.standardResult(maxCursor, minCursor - maxCursor + 1);
            }

            checkResultLimit(paging, maxResultAllowed);
        } else if (LAST_RESULTS_PATTERN.matcher(range).matches()) {
            paging = Paging.lastResults(Math.abs(Integer.parseInt(range)));

            checkResultLimit(paging, maxResultAllowed);
        } else {
            paging = Paging.standardResult(0, Integer.MAX_VALUE - 1);
        }

        return paging;
    }

    private static void checkResultLimit(final Paging paging, final int maxResultAllowed) {

        if (paging.limit() > maxResultAllowed) {
            throw new AskForToMuchResultRuntimeException("Client requests to much results, " + paging.limit()  + " asked, " + maxResultAllowed + " allowed.");
        }
    }
}
