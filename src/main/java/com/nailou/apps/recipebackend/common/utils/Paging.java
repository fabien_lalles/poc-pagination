package com.nailou.apps.recipebackend.common.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.domain.Sort;

public record Paging(int offset, int limit, Sort.Direction direction) {

    public Paging(final int offset, final int limit, final Sort.Direction direction) {
        this.offset = offset;
        this.limit = limit - offset;
        this.direction = direction;
    }

    public static Paging lastResults(final int limit) {
        return new Paging(0, limit, Sort.Direction.DESC);
    }

    public static Paging standardResult(final int offset, final int limit) {
        return new Paging(offset, limit, Sort.Direction.ASC);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("offset", offset)
                .append("limit", limit)
                .append("direction", direction)
                .toString();
    }
}
